\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		c2. c2.

		% señor, ten piedad...
		c2. c2. e2.:m e2.:m
		f2. f2. g2. g2.
		c2. c2. e2.:m e2.:m
		f2. f2. g2. g2.

		% cristo, ten piedad...
		a2.:m a2.:m c2. c2.
		a2.:m a2.:m f2. f2.
		d2. d2. g2. f2. e2.:m d2.:m

		% señor, ten piedad...
		c2. c2. e2.:m e2.:m
		f2. f2. g2. g2.
		c2. c2. e2.:m e2.:m
		f2. f2. g2. g2.
		c2. c2.
	}
