\context Staff = "mezzosoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 6/8
		\clef "treble"
		\key c \major

		R2.  |
		r2 g' 4  |
		g' 2. ~  |
		g' 8 r g' 4 g'  |
%% 5
		g' 2. ~  |
		g' 4. r8 g' 4  |
		f' 2. ~  |
		f' 4 e' f'  |
		d' 2. ~  |
%% 10
		d' 4. r8 e' 4  |
		e' 2. ~  |
		e' 8 r e' 4 e'  |
		d' 2. ~  |
		d' 4. r8 d' 4  |
%% 15
		c' 2. ~  |
		c' 4 a c'  |
		d' 2. ~  |
		d' 4. r  |
		e' 2.  |
%% 20
		e' 4 e' d'  |
		c' 2. ~  |
		c' 2 r8 r  |
		e' 2.  |
		e' 4 a' g'  |
%% 25
		f' 2. ~  |
		f' 2.  |
		fis' 2.  |
		fis' 4 e' fis'  |
		g' 2.  |
%% 30
		f' 2.  |
		e' 2.  |
		d' 4. r8 e' 4  |
		e' 2. ~  |
		e' 8 r e' 4 e'  |
%% 35
		d' 2. ~  |
		d' 4. r8 d' 4  |
		c' 2. ~  |
		c' 4 a c'  |
		d' 2. ~  |
%% 40
		d' 4. r8 e' 4  |
		e' 2. ~  |
		e' 8 r e' 4 e'  |
		d' 2. ~  |
		d' 4. r8 d' 4  |
%% 45
		c' 2. ~  |
		c' 4 a c'  |
		d' 2. ( ~  |
		d' 2.  |
		c' 2. ~  |
%% 50
		c' 2. )  |
		R2.  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzosoprano" {
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Cris -- to, ten pie -- dad. __
		Cris -- to, ten pie -- dad. __
		Cris -- to, ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
	}
>>
