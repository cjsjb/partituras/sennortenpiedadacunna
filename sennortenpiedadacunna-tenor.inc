\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 6/8
		\clef "treble_8"
		\key c \major

		R2.  |
		r2 g 4  |
		g 2. ~  |
		g 8 r g 4 g  |
%% 5
		g 2. ~  |
		g 4. r8 g 4  |
		f 2. ~  |
		f 4 e f  |
		d 2. ~  |
%% 10
		d 4. r8 c' 4  |
		c' 2. ~  |
		c' 8 r c' 4 c'  |
		b 2. ~  |
		b 4. r8 b 4  |
%% 15
		a 2. ~  |
		a 4 a a  |
		b 2. ~  |
		b 4. r  |
		e 2.  |
%% 20
		e 4 e d  |
		c 2. ~  |
		c 2 r8 r  |
		e 2.  |
		e 4 a g  |
%% 25
		f 2. ~  |
		f 2.  |
		a 2.  |
		a 4 a a  |
		b 2.  |
%% 30
		a 2.  |
		b 2. ~  |
		b 4. r8 g 4  |
		g 2. ~  |
		g 8 r g 4 g  |
%% 35
		g 2. ~  |
		g 4. r8 g 4  |
		f 2. ~  |
		f 4 e f  |
		d 2. ~  |
%% 40
		d 4. r8 c' 4  |
		c' 2. ~  |
		c' 8 r c' 4 c'  |
		b 2. ~  |
		b 4. r8 b 4  |
%% 45
		a 2. ~  |
		a 4 a a  |
		b 2. ( ~  |
		b 2.  |
		c' 2. ~  |
%% 50
		c' 2. )  |
		R2.  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Cris -- to, ten pie -- dad. __
		Cris -- to, ten pie -- dad. __
		Cris -- to, ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
		Se -- ñor, __ ten pie -- dad. __
	}
>>
